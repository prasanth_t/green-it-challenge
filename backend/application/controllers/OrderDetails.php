<?php

/**
 * Die if not called via Base path.
 */

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * OrderDetails Controller which extends default CI_Controller.
 */
class OrderDetails extends CI_Controller
{
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/greenit
     *  - or -
     *      http://example.com/index.php/GreenITOrders/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/userguide3/general/urls.html
     */
    public function index()
    {
        $this->load->view('order_details_message');
    }

    /**
     * Set Headers for Pass & Obtain the orders between frontend framework.
     */
    private function setHeadersAndModel()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        header('Content-Type: application/json; charset=UTF-8');
        // Load Model.
        $this->load->model('OrderDetailsModel');
    }

    /**
     * Read the Orders from Model.
     *
     * @return string jsondata.
     */
    public function readOrders()
    {
        $this->setHeadersAndModel();
        echo json_encode($this->OrderDetailsModel->getEntries());
    }

    /**
     * Check and Pass the $id to Model which is unset from CSV file.
     *
     * @param number $id id of the Entry.
     *
     * @return string http_response_code.
     */
    public function deleteOrder($id)
    {

        $this->setHeadersAndModel();
        // Extract, validate and sanitize the id.
        $id = (isset($id) && $id !== null && (int)$id > 0) ? (int)$id : false;

        /**
         * Return 400 response if Id is undefined.
         */
        if (!$id) {
            echo http_response_code(400);
        } else {
            /**
             * Unset the Entry using $id.
             */
            $this->OrderDetailsModel->deleteOrder($id);
        }
    }

    /**
     * Update the orders in Model
     *
     * @return string http_response_code
     */
    public function updateOrders()
    {
        $this->setHeadersAndModel();
        // Get the posted orders and Decode it.
        $postdata = json_decode(file_get_contents("php://input"), true);

        /**
         * Pass the orders to be updated which is returned by Frontend Framework.
         */
        if ($this->validateOrders($postdata)) {
            $this->updateEntries($postdata);
        } else {
            /**
             * Return 400 Bad request for Invalid orders.
             */
            return http_response_code(400);
        }
    }

    /**
     * Update the valid Data in Model and CSV.
     *
     * @param array $orders orders to updated in Model and CSV.
     */
    public function updateEntries($orders)
    {
        // Load Model.
        $this->load->model('OrderDetailsModel');
        $this->OrderDetailsModel->updateOrders($orders);
    }

    /**
     * Validate the orders in array format.
     *
     * @param array $orders to be validate using Regular Expression.
     *
     * @return bool validated or not.
     */
    public function validateOrders($orders)
    {
        if (is_array($orders)) {
            if (!empty($orders)) {
                foreach ($orders as $each_order) {
                    $float_value = (float) $each_order['amount'];
                    /**
                     * validate for multiple checks
                     */
                    if (!isset($each_order['id']) || !isset($each_order['name']) || !isset($each_order['state']) ||
                    !isset($each_order['zip']) || !isset($each_order['amount']) || !isset($each_order['qty']) ||
                    !isset($each_order['item'])) {
                        // Return 400 Bad Request each_order value is not set.
                        return false;
                    } elseif ('' == $each_order['id'] || '' == $each_order['name'] || '' == $each_order['state'] ||
                    '' == $each_order['zip'] || '' == $each_order['amount'] || '' == $each_order['qty'] ||
                    '' == $each_order['item']) {
                        // Return 400 Bad Request each_order value is string null.
                        return false;
                    } elseif (5 != strlen($each_order['zip']) && 2 != strlen($each_order['state'])) {
                        // Return 400 Bad Request zip length is not five and state value is not two.
                        return false;
                    } elseif (!preg_match("/^\d+$/", $each_order['id']) || !preg_match("/^\d+$/", $each_order['zip']) ||
                    !preg_match("/^\d+$/", $each_order['qty'])) {
                        // Return 400 Bad Request id, zip and quantity is not number.
                        return false;
                    } elseif (strval($float_value) != $each_order['amount']) {
                        // Return 400 Bad Request amount is not float.
                        return false;
                    }
                }
            } else {
                // Allow to save empty orders.
                return true;
            }
        } else {
            // Don't allow the orders when not if array format.
            return false;
        }
        // Validation done after passing above conditions.
        return true;
    }
}

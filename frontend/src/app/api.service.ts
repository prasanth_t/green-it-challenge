
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Orders } from './orders';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  // Backend URI.
  PHP_API_SERVER = "http://localhost/green-it-challenge/backend/index.php/OrderDetails";

  constructor(private httpClient: HttpClient) {}

  // Read Orders from Backend using Rest API.
  readOrders(): Observable<Orders>{
    return this.httpClient.get<Orders>(`${this.PHP_API_SERVER}/readOrders`);
  }

  // Update the entries then pass to Backend.
  updateOrders(data: any): Observable<any>{
    let jsonData = JSON.stringify(data);
    return this.httpClient.post<Orders>(`${this.PHP_API_SERVER}/updateOrders`, jsonData, {responseType: 'json'});   
  }

  // Delete the entry using Id.
  deleteOrder(id: number): Observable<any>{
    return this.httpClient.delete<Orders>(`${this.PHP_API_SERVER}/deleteOrder/${id}`);
  }

}


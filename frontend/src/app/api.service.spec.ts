import { TestBed } from '@angular/core/testing';

import { ApiService } from './api.service';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('ApiService', () => {

  let service: ApiService;

  let httpMock: HttpTestingController;

  let PHP_API_SERVER = "http://localhost/green-it-challenge/backend/index.php/OrderDetails";

  let testOrders: any[] = [
    {id:"1", name:"Liquid Saffron", state:"NY", zip:"08998", amount:"25.43", qty:"7", item:"XCD45300"},
    {id:"2", name:"Mostly Slugs", state:"PA", zip:"19008", amount:"13.30", qty:"2", item:"AAH6748"},
    {id:"3", name:"Jump Stain", state:"CA", zip:"99388", amount:"56.00", qty:"3", item:"MKII4400"},
    {id:"4", name:"Scheckled Sherlock", state:"WA", zip:"88990", amount:"987.56", qty:"4", item:"TR909"},
    {id:"5", name:"Prasanth T", state:"TN", zip:"63150", amount:"87.65", qty:"8", item:"TN01"}
  ];

  let deleteId: number = 5;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(ApiService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('Should be created', () => {
    expect(service).toBeTruthy();
  });

  it('Should return the result', () => {
    service.readOrders().subscribe( result => {
      expect(result).toBeTruthy();
    });
    const request = httpMock.expectOne(PHP_API_SERVER+'/readOrders');
    expect(request.request.method).toBe('GET');
    request.flush(testOrders);
  });

  it('Should delete the entry by ID', () => {
    service.deleteOrder(deleteId).subscribe( result => {
      expect(result).toBeTruthy();
    });
    const request = httpMock.expectOne(PHP_API_SERVER+'/deleteOrder/'+deleteId);
    expect(request.request.method).toBe('DELETE');
    request.flush(testOrders);
  });

  it('Should update the entries', () => {
    service.updateOrders(testOrders).subscribe( result => {
      expect(result).toBeTruthy();
    });
    const request = httpMock.expectOne(PHP_API_SERVER+'/updateOrders');
    expect(request.request.method).toBe('POST');
    request.flush(testOrders);
  });
});

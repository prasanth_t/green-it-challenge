import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'GreenIT Application Challenge'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('GreenIT Application Challenge');
  });

  it(`should have toggle the Sucess status Boolean`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    app.toggleSuccessMessage(true);
    expect(app.showSaved).toBeTruthy();
  });

  it(`should have toggle the Error status Boolean`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    app.toggleErrorMessage(true);
    expect(app.notSaved).toBeTruthy();
  });

  it(`should have set the Error Message 'Can not Save!'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    app.ErrorMessageWhenSave('Can not Save!');
    expect(app.cantSaveError).toEqual('Can not Save!');
  });

  it(`should have set the Error Message 'Permission Denied!'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    app.ErrorMessage('Permission Denied!');
    expect(app.errorMessage).toEqual('Permission Denied!');
  });

});

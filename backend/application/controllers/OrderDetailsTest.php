<?php

/**
 * Die if not called via Base path.
 */
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * OrderDetailsTest Controller which extends default CI_Controller.
 */
class OrderDetailsTest extends CI_Controller
{
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/OrderDetailsTest/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/userguide3/general/urls.html
     */
    public function index()
    {
        $this->load->view('order_details_message');
    }

    /**
     * Read the Orders from Model.
     *
     * @return string jsondata.
     */
    public function readOrders()
    {
        $this->load->model('OrderDetailsModel');
        return $this->OrderDetailsModel->getEntries();
    }

    /**
     * Check and Pass the $id to Model which is unset from CSV file.
     *
     * @param number $id id of the Entry.
     *
     * @return string http response code.
     */
    public function deleteOrder($id)
    {

        $this->load->model('OrderDetailsModel');
        // Extract, validate and sanitize the id.
        $id = (isset($id) && $id !== null && (int)$id > 0)? (int)$id : false;

        /**
         * Return 400 response if Id is undefined.
         */
        if (!$id) {
            echo http_response_code(400);
        } else {
            /**
             * Unset the Entry using $id.
             */
            $this->OrderDetailsModel->deleteOrder($id);
        }
    }

    /**
     * Update the valid Data in Model and CSV.
     *
     * Params array $orders orders to updated in Model and CSV.
     */
    public function updateEntries($orders)
    {
        // Load Model.
        $this->load->model('OrderDetailsModel');
        $this->OrderDetailsModel->updateOrders($orders);
    }

    /**
     * Validate the orders in array format.
     *
     * Params array $orders to be validate using Regular Expression.
     */
    public function validateOrders($orders)
    {
        if (is_array($orders)) {
            if (!empty($orders)) {
                foreach ($orders as $each_order) {
                    $float_value = (float) $each_order['amount'];
                    /**
                     * validate for multiple checks
                     */
                    if (!isset($each_order['id']) || !isset($each_order['name']) || !isset($each_order['state']) ||
                    !isset($each_order['zip']) || !isset($each_order['amount']) || !isset($each_order['qty']) ||
                    !isset($each_order['item'])) {
                        // Return 400 Bad Request each_order value is not set.
                        return false;
                    } elseif ('' == $each_order['id'] || '' == $each_order['name'] || '' == $each_order['state'] ||
                    '' == $each_order['zip'] || '' == $each_order['amount'] || '' == $each_order['qty'] ||
                    '' == $each_order['item']) {
                        // Return 400 Bad Request each_order value is string null.
                        return false;
                    } elseif (5 != strlen($each_order['zip']) && 2 != strlen($each_order['state'])) {
                        // Return 400 Bad Request zip length is not five and state value is not two.
                        return false;
                    } elseif (!preg_match("/^\d+$/", $each_order['id']) || !preg_match("/^\d+$/", $each_order['zip']) ||
                    !preg_match("/^\d+$/", $each_order['qty'])) {
                        // Return 400 Bad Request id, zip and quantity is not number.
                        return false;
                    } elseif (strval($float_value) != $each_order['amount']) {
                        // Return 400 Bad Request amount is not float.
                        return false;
                    }
                }
            } else {
                // Allow to save empty orders.
                return true;
            }
        } else {
            // Don't allow the orders when not if array format.
            return false;
        }
        // Validation done after passing above conditions.
        return true;
    }

    /**
     * Test read orders with phpunit
     */
    public function testReadOrders()
    {
        echo json_encode($this->readOrders());
    }

    /**
     * Test invalid orders with phpunit
     */
    public function testInvalidOrders()
    {
        $invalidOrders = array(
            array('id' => 1, 'name' => '', 'state' => 'NY', 'zip' => '08998',
            'amount' => '25.43', 'qty' => '7', 'item' => 'XCD45300'),
            array('id' => 2, 'name' => 'Mostly Slugs', 'state' => 'PA', 'zip' => '19008',
            'amount' => '13.30', 'qty' => '2', 'item' => 'AAH6748'),
            array('id' => 3, 'name' => 'Jump Stain', 'state' => 'CA', 'zip' => '99388',
            'amount' => '56.00', 'qty' => '3', 'item' => 'MKII4400'),
            array('id' => 4, 'name' => 'Scheckled Sherlock', 'state' => 'WA', 'zip' => '88990',
            'amount' => '987.56', 'qty' => '4', 'item' => 'TR909'),
        );
        echo $this->validateOrders($invalidOrders);
    }

    /**
     * Test valid orders with phpunit
     */
    public function testValidationOfOrders()
    {
        $validOrders = array(
            array('id' => 1, 'name' => 'Liquid Saffron', 'state' => 'NY', 'zip' => '08998',
            'amount' => '25.43', 'qty' => '7', 'item' => 'XCD45300'),
            array('id' => 2, 'name' => 'Mostly Slugs', 'state' => 'PA', 'zip' => '19008',
            'amount' => '13.30', 'qty' => '2', 'item' => 'AAH6748'),
            array('id' => 3, 'name' => 'Jump Stain', 'state' => 'CA', 'zip' => '99388',
            'amount' => '56.00', 'qty' => '3', 'item' => 'MKII4400'),
            array('id' => 4, 'name' => 'Scheckled Sherlock', 'state' => 'WA', 'zip' => '88990',
            'amount' => '987.56', 'qty' => '4', 'item' => 'TR909'),
        );
        echo $this->validateOrders($validOrders);
    }

    /**
     * Test update orders with phpunit
     */
    public function testUpdateOrders()
    {
        $Orders = array(
            array('id' => 1, 'name' => 'Liquid Saffron', 'state' => 'NY', 'zip' => '08998',
            'amount' => '25.43', 'qty' => '7', 'item' => 'XCD45300'),
            array('id' => 2, 'name' => 'Mostly Slugs', 'state' => 'PA', 'zip' => '19008',
            'amount' => '13.30', 'qty' => '2', 'item' => 'AAH6748'),
            array('id' => 3, 'name' => 'Jump Stain', 'state' => 'CA', 'zip' => '99388',
            'amount' => '56.00', 'qty' => '3', 'item' => 'MKII4400'),
            array('id' => 4, 'name' => 'Scheckled Sherlock', 'state' => 'WA', 'zip' => '88990',
            'amount' => '987.56', 'qty' => '4', 'item' => 'TR909'),
            array('id' => 5, 'name' => 'Prasanth T', 'state' => 'TN', 'zip' => '63150',
            'amount' => '123.45', 'qty' => '6', 'item' => 'AP786'),
        );
        echo $this->updateEntries($Orders);
    }

    /**
     * Test delete order with phpunit
     */
    public function testDeleteOrders()
    {
        $id = 5;
        $this->load->model('OrderDetailsModel');
        $array = $this->readOrders();
        if (array_key_exists($id, $array)) {
            echo $this->deleteOrder($id);
        } else {
            echo 400;
        }
    }

    /**
     * Test delete not existing order with phpunit
     */
    public function testInvalidDeleteOrders()
    {
        $id = 50;
        $this->load->model('OrderDetailsModel');
        $array = $this->readOrders();
        if (array_key_exists($id, $array)) {
            echo $this->deleteOrder($id);
        } else {
            echo 400;
        }
    }
}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TableComponent } from './table/table.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CustomTooltipComponent } from './custom-tooltip/custom-tooltip.component';

import { AgGridModule} from 'ag-grid-angular';
import { OrderInterceptor} from './orders.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    CustomTooltipComponent
  ],
  imports: [
    BrowserModule,
    AgGridModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule
  ],
  providers: [{provide:HTTP_INTERCEPTORS,useClass: OrderInterceptor,multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  title:string = 'GreenIT Application Challenge';

  showSaved: boolean = false; // Boolean to show and hide Success Message.

  notSaved: boolean =false; // Boolean to show Error Messages when problem with Backend.

  cantSaveError: string = ''; // Boolean to show and hide Error Message when try to delete or update entries.

  errorMessage: string = '';  // String Error Message from Interceptor.

  validationMessage: string = '';  // String Validation Error Message.

  ngOnInit(){} // Called after Constructor.

  toggleSuccessMessage(val: boolean) {
    this.showSaved = val; // Toggle the boolean to show & hide the Success Message.
  }

  toggleErrorMessage(val: boolean) {
    this.notSaved = val; // Toggle the boolean to show the Error Message.
  }

  ErrorMessageWhenSave(val: string){
    this.cantSaveError = val; // Set Error Message when delete or update entries from Interceptor.
  }
  
  ErrorMessage(val: string){
    this.errorMessage = val; // Set Error Message when issue with Backend, from Interceptor.
  }

  ValidationMessage(val: string){
    let message = val.replace(/\n/g, '<br>');
    this.validationMessage = message; // Set Validation Error Message.
  }
}

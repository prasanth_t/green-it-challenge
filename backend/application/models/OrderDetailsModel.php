<?php
/**
 * OrderDetailsModel Model which extends default CI_Model.
 */
class OrderDetailsModel extends CI_Model
{
    /**
     * @var string $file_name file name of CSV file holds orders.
     */
    private $file_name = SYSDIR . '/orders.csv';

    /**
     * @var array $orders_arr Orders stored in array which is connects the frontend via Controller and CSV.
     */
    private $orders_arr = array();

    /**
     * Get the Entries in CSV as Array.
     *
     * @return array $orders_arr orders from CSV.
     */
    public function getEntries()
    {

        /**
         * Open File.
         */
        if (!file_exists($this->file_name)) {
            fopen($this->file_name, 'w');
            return $this->orders_arr;
        } else {
            $file_to_read = fopen($this->file_name, 'r');
            if (!$file_to_read) {
                /**
                 * File Open Failed.
                 */
                echo http_response_code(403);
            } else {
                $row = 0;
                /**
                 * Iterate the lines of CSV.
                 */
                while (($orders = fgetcsv($file_to_read, 100, ',')) !== false) {
                    if ($row == 0) {
                        $keys = $orders;
                    } else {
                        for ($i = 0; $i < count($orders); $i++) {
                            if (array_key_exists($i, $keys)) {
                                // Store each entry in Orders array $orders_arr variable.
                                $this->orders_arr[$row][$keys[$i]] = $orders[$i];
                            }
                        }
                    }
                    $row++;
                }
                /**
                 * Close the file.
                 */
                fclose($file_to_read);
            }
            // Return Entries
            return $this->orders_arr;
        }
    }

    /**
     * Delete the Entry using Id.
     *
     * @param int $id which will be deleted.
     *
     * @return string response code .
     */
    public function deleteOrder($id)
    {
        $this->getEntries();
        if (is_array($this->orders_arr)) {
            foreach ($this->orders_arr as $key => $value) {
                if ($id == $value['id']) {
                    /**
                     * Unset the entry from an Array.
                     */
                    unset($this->orders_arr[$key]);
                }
            }
            /**
             * Update the array in to CSV.
             */
            return $this->updateOrders($this->orders_arr);
        } else {
            return http_response_code(400);
        }
    }

    /**
     * Update the post orders as entries in CSV file.
     *
     * @param array $post_orders.
     *
     * @return string Response code.
     */
    public function updateOrders($post_orders)
    {
        $this->orders_arr = $post_orders;
        array_unshift($post_orders, array('id', 'name', 'state', 'zip', 'amount', 'qty', 'item'));

        try {
            $file_to_read = fopen($this->file_name, 'w');
            if (!$file_to_read) {
                /**
                 * File Open Failed.
                 */
                throw new Exception("Not Allowed. ", 403);
            } else {
                /**
                 * Return the file handler.
                 */
                foreach ($post_orders as $value) {
                    // insert the each array in to each line.
                    fputcsv($file_to_read, $value);
                }
                // Close the file.
                fclose($file_to_read);
                // Send Success response code.
                echo http_response_code(200);
            }
        } catch (Exception $e) {
            // Send error response codes.
            return http_response_code($e->getCode());
        }
    }
}

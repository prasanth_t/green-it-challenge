import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, catchError, throwError } from 'rxjs';

@Injectable()
export class OrderInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      catchError(errorData => {
        let errorMessage = '';
        if(errorData.status == 404){
          errorMessage = 'OOPS ! Not Found.';
        }else if(errorData.status == 400){
          errorMessage = 'OOPS ! Bad Request.';
        }else if(errorData.status == 403){
          errorMessage = 'OOPS ! Not Allowed.';
        }else {
          errorMessage = 'OOPS ! Server Down.';
        }
        return throwError(errorMessage);
      })
    );
  }
}

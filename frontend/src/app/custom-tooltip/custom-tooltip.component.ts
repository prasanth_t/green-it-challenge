
import { Component } from '@angular/core';
import { ITooltipAngularComp } from 'ag-grid-angular';
import { ITooltipParams } from 'ag-grid-community';

@Component({
  selector: 'app-custom-tooltip',
  templateUrl: './custom-tooltip.component.html',
  styleUrls: ['./custom-tooltip.component.css']
}) export class CustomTooltipComponent implements ITooltipAngularComp {

  constructor(){}

  private params!: { color: string } & ITooltipParams;

  public order!: any;

  public color!: string;

  agInit(params: { color: string } & ITooltipParams): void {

    this.params = params;

    this.order = params.api!.getDisplayedRowAtIndex(params.rowIndex!)!.data;
    
    this.color = this.params.color || 'white';
  }
}

<?php

/**
 * OrderDetails_Test Testcase using phpunit.
 */
class TestOrderDetails extends TestCase
{
    /**
     * Initialize the Class.
     */
    public function __construct()
    {
        parent::__construct();
        // Load CI application
        $this->CI = & get_instance();
    }

    /**
     * Test the Config routes the Index.
     */
    public function testConfigItem()
    {
        $indexPage = $this->CI->config->item('index_page');
        $this->assertSame('index.php', $indexPage);
    }

    /**
     * Return 404 for undefined methods.
     */
    public function testMethod404()
    {
        $this->request('GET', ['OrderDetailsTest','method_not_exist']);
        $this->assertResponseCode(404);
    }

    /**
     * Checks the Title of the Application.
     */
    public function testContainsTitle()
    {
        $output = $this->request('GET', ['OrderDetailsTest','index']);
        $expected = 'Green IT Application Challenge';
        $this->assertStringContainsString($expected, $output);
    }

    /**
     * Checks the Data, pass the orders to frontend in Json format.
     */
    public function testJsonOrders()
    {
        $output = $this->request('POST', ['OrderDetailsTest', 'testReadOrders']);
        $this->assertIsString($output);
    }

    /**
     * Checks for valid orders else return 400 Bad Request.
     */
    public function testValidateOrders()
    {
        $output = $this->request('POST', ['OrderDetailsTest', 'testValidationOfOrders']);
        $this->assertEquals(1, $output);
    }

    /**
     * Checks for invalid orders else return 400 Bad Request.
     */
    public function testInValidateOrders()
    {
        $output = $this->request('POST', ['OrderDetailsTest', 'testInvalidOrders']);
        $this->assertEquals("", $output);
    }

    /**
     * Checks for update the orders.
     */
    public function testUpdateOrders()
    {
        $output = $this->request('POST', ['OrderDetailsTest', 'testUpdateOrders']);
        $this->assertEquals("1", $output);
    }

    /**
     * Checks for delete the order.
     */
    public function testDeleteOrders()
    {
        $output = $this->request('DELETE', ['OrderDetailsTest', 'testDeleteOrders']);
        $this->assertEquals("200", $output);
    }

    /**
     * Checks for delete the not existing order.
     */
    public function testDeleteInvalidOrders()
    {
        $output = $this->request('POST', ['OrderDetailsTest', 'testInvalidDeleteOrders']);
        $this->assertEquals("400", $output);
    }
}

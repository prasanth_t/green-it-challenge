import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { CellValueChangedEvent,
  ColDef,
  CellClassRules,
  RowSelectedEvent, 
  CellEditingStartedEvent,
  CellEditingStoppedEvent,
  GridReadyEvent} from 'ag-grid-community';
import { Router } from '@angular/router';
import { AgGridAngular } from 'ag-grid-angular';
import { ApiService } from '../api.service';
import { CustomTooltipComponent } from '../custom-tooltip/custom-tooltip.component';
import { Orders } from '../orders';
import Swal from 'sweetalert2'; 

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})export class TableComponent implements OnInit {

  constructor(private apiOrders: ApiService, private router: Router){}

  // Receives the Title from Parent Component.
  @Input() title:any;

  // Update the Boolean toggleSuccess in Parent Component.
  @Output() toggleSuccess = new EventEmitter<boolean>();

  // Update the Boolean toggleError in Parent Component.
  @Output() toggleError = new EventEmitter<boolean>();

  // Update the String cantSaveError in Parent Component.
  @Output() cantSaveError = new EventEmitter<string>();

  // Update the String errorMessage in Parent Component.
  @Output() errorMessage = new EventEmitter<string>();

  // Update the String validationMessage in Parent Component.
  @Output() validationMessage = new EventEmitter<string>();

  ngOnInit(): void {} // Called after Constructor.

  // Access and Use the library of ag-grid package.
  @ViewChild(AgGridAngular) agGrid!: AgGridAngular;

  // Load table called after Init.
  onGridReady(params: GridReadyEvent<Orders>) {
    this.fetchFromServer();
    this.agGrid.api.sizeColumnsToFit();
  }

  isSaveAttempted:boolean = false;

  validErrorMessage:string = '';

  items:{ [key: string]: number } = {};

  toggleAddSave:boolean = false;

  rowOrders: Orders[] = [];

  tooltipShowDelay:number = 0;

  tooltipHideDelay:number = 2000;

  ordersToSave: any[] = [];

  validationPass:boolean = true;

  cellValueChanged:boolean = false;

  cellEditStopped:boolean = true;

  deleted: boolean = false;

  selectedCount:number = 0;

  valueChangedIds:number[] = [];

  EmptyMessage:string = '';
  NameMessage:string = '';
  StateMessage:string = '';
  ZIPMessage:string = '';
  AmountMessage:string = '';
  QuantityMessage:string = '';
  ItemMessage:string = '';
  ItemUniqueMessage:string = '';

  // Table Cell ClassRules for accepting input is not null.
  stringRules: CellClassRules = {
    'rag-red': (params) => this.isSaveAttempted && (params.value == '' || !/^[a-zA-Z]+$/.test(params.value.replace(/\s/g, "")))
  };

  // Table Cell ClassRules for accepting state input.
  stateRules: CellClassRules = {
    'rag-red': (params) => this.isSaveAttempted && (params.value == '' || params.value.length != 2 || !/^[A-Z]+$/.test(params.value))
  };

  // Table Cell ClassRules for accepting zip input.
  zipRules: CellClassRules = {
    'rag-red': (params) => this.isSaveAttempted && (params.value == '' || params.value.length != 5 || isNaN(params.value) || parseInt(params.value) <= 0) 
  };

  // Table Cell ClassRules for accepting numberic input.
  numberRules: CellClassRules = {
    'rag-red': (params) => this.isSaveAttempted && (params.value == '' || isNaN(params.value) ||  parseFloat(params.value) <= 0) 
  }

  // Table Cell ClassRules for accepting item is not null and unique.
  itemRules: CellClassRules = {
    'rag-red': (params) => this.isSaveAttempted && (params.value == '' || this.items[params.value] > 1 || !/^[a-zA-Z0-9]+$/.test(params.value))
  };

  // Default attribute of ag-grid Table.
  defaultColDef: ColDef = {
    sortable: true, filter: true, tooltipComponent: CustomTooltipComponent // Load CustomTooltipComponent as Sibling.
  }

  // Columns attribute of ag-grid Table.
  columnDefs: ColDef[] = [
    { headerName: 'ID',
      field: 'id',
      headerCheckboxSelection: true,
      checkboxSelection: true,
      showDisabledCheckboxes: true },
    { headerName: 'Name',
      field: 'name',
      editable: true,
      tooltipField: 'name',
      tooltipComponentParams: { color: '#ececec' },
      cellClassRules: this.stringRules},
    { headerName: 'State', 
      field: 'state',
      editable: true,
      cellClassRules: this.stateRules},
    { headerName: 'Amount',
      field: 'amount',
      editable: true,
      cellClassRules: this.numberRules},
    { headerName: 'ZIP code',
      field: 'zip',
      editable: true,
      cellClassRules: this.zipRules},
    { headerName: 'Quantity',
      field: 'qty',
      editable: true,
      cellClassRules: this.numberRules},
    { headerName: 'Item',
      field: 'item',
      editable: true,
      cellClassRules: this.itemRules},
  ]

  // Assign Checkbox Selection to ID Column.
  autoGroupColumnDef = {
    headerName: 'ID',
    field: 'id',
    cellRenderer: 'agGroupCellRenderer',
    cellRendererParams: { checkbox: true },
  };

  // Fetch the orders from Backend.
  fetchFromServer(){
    this.apiOrders.readOrders().subscribe((res:Orders) => {
      this.rowOrders = Array.from(Object.values(res)); // Fetched to Table.
    }, err => {
      this.rowOrders = [];
      this.errorMessage.emit(err); // Can't Fetch, Issue with Backend.
    });
  }

  // Update the Count of Selected Rows.
  onRowSelected(event: RowSelectedEvent){
    var selectedOrders = this.agGrid.api.getSelectedRows ();
    this.selectedCount = selectedOrders.length;
  }

  // Cell Value changed event.
  onCellValueChanged(event: CellValueChangedEvent){
    this.getAllRecords();
    this.cellValueChanged = true;
    this.toggleAddSave = true;
    this.valueChangedIds.push(event.data['id']);
  }

  // Disable update when Editing.
  onCellEditingStarted(event: CellEditingStartedEvent){
    this.cellEditStopped = false;
  }

  // Allow update After Edit.
  onCellEditingStopped(event: CellEditingStoppedEvent){
    this.cellEditStopped = true;
  }

  /**
   * Get entries from ag-grid Table.
   */
  getAllRecords(){
    this.validationPass = true;
    this.ordersToSave = [];
    this.items = {};
    this.validErrorMessage = '';
    this.EmptyMessage = '';
    this.NameMessage = '';
    this.StateMessage = '';
    this.AmountMessage = '';
    this.ZIPMessage = '';
    this.QuantityMessage = '';
    this.ItemMessage = '';
    this.ItemUniqueMessage = '';
    this.validationMessage.emit('');    
    this.agGrid.api.forEachNode((rowNode) => {
      if('' != rowNode.data['amount'].toString().replace(/\s/g, "") && !isNaN(rowNode.data['amount'])){
        rowNode.setDataValue('amount', parseFloat(rowNode.data['amount']).toFixed(2));
      }
      if('' != rowNode.data['qty'].toString().replace(/\s/g, "")){
        rowNode.setDataValue('qty', parseInt(rowNode.data['qty']));
      }
      if('' != rowNode.data['zip'].toString().replace(/\s/g, "")){
        rowNode.setDataValue('zip', parseInt(rowNode.data['zip']));
      }
      rowNode.setDataValue('state', rowNode.data['state'].toUpperCase());
      rowNode.setDataValue('item', rowNode.data['item'].toUpperCase());
      if(!this.items[rowNode.data['item']]){
        this.items[rowNode.data['item']] = 1;
      }else {
        this.items[rowNode.data['item']] = this.items[rowNode.data['item']]+1;
      }

      /**
       * Validation for Each Column
       */
      this.clientSideValidation(rowNode);
      // Save entries to row Node when insert, delete and update
      this.ordersToSave.push(rowNode.data);
    });
  }

  clientSideValidation(rowNode:any){

    if ('' == rowNode.data['name'].replace(/\s/g, "") || '' == rowNode.data['item'].replace(/\s/g, "") || '' == rowNode.data['amount'].toString().replace(/\s/g, "") ||
      '' == rowNode.data['qty'].toString().replace(/\s/g, "") || '' == rowNode.data['zip'].toString().replace(/\s/g, "") ||
      '' == rowNode.data['state'].replace(/\s/g, "") ){
        this.validationPass = false;
        this.EmptyMessage = "Highlighted Cell Can't be empty. ";
    }
    if(!/^[a-zA-Z]+$/.test(rowNode.data['name'].replace(/\s/g, "")) && '' != rowNode.data['name'].replace(/\s/g, "")){
      this.validationPass = false;
      this.NameMessage = "Please enter a valid Name.\n";
    }
    if((!/^[A-Z]+$/.test(rowNode.data['state']) || rowNode.data['state'].length != 2)  && '' != rowNode.data['state'].replace(/\s/g, "")){
      this.validationPass = false;
      this.StateMessage = "Please enter valid alphabetic two digit State code.\n";
    }
    if((rowNode.data['zip'].toString().length != 5 || isNaN(rowNode.data['zip']) || rowNode.data['zip'] <= 0)  && '' != rowNode.data['zip'].replace(/\s/g, "")){
      this.validationPass = false;
      this.ZIPMessage = "Please enter valid numeric five digit ZIP code.\n";
    }
    if((isNaN(rowNode.data['amount']) || parseFloat(rowNode.data['amount'].toString()) <= 0) && '' != rowNode.data['amount'].replace(/\s/g, "")){
      this.validationPass = false;
      this.AmountMessage = "Please enter valid Amount.\n";
    }
    if((isNaN(rowNode.data['qty']) || rowNode.data['qty'] <= 0)  && '' != rowNode.data['qty'].replace(/\s/g, "")){
      this.validationPass = false;
      this.QuantityMessage = "Quantity should be a valid number.\n";
    }
    if((!/^[a-zA-Z0-9]+$/.test(rowNode.data['item']))  && '' != rowNode.data['item'].replace(/\s/g, "")){
      this.validationPass = false;
      this.ItemMessage = "Please enter valid Item.\n";
    }
    if((this.items[rowNode.data['item']] > 1) && '' != rowNode.data['item'].replace(/\s/g, "")){
      this.validationPass = false;
      this.ItemUniqueMessage = "Item should be unique.\n";
    }
    // Display Error Messages.
    this.validErrorMessage = this.EmptyMessage+this.NameMessage+this.StateMessage+
    this.ZIPMessage+this.AmountMessage+this.QuantityMessage+
    this.ItemMessage+this.ItemUniqueMessage;
    if(this.isSaveAttempted && !this.validationPass){
      this.validationMessage.emit(this.validErrorMessage);
    }
  }

  // Insert the entry with empty values.
  onAddRow(){
    this.getAllRecords();
    if(this.ordersToSave.length > 0){
      var lastId = this.ordersToSave[this.ordersToSave.length - 1]['id'];
      var id = parseInt(lastId)+1;
    }else {
      var id = 1;
    }
    this.agGrid.api.applyTransaction({add: [{id: id, name: '', state: '', zip: '', amount: '', qty: '', item: '', newlyAdded: true}]});
    this.toggleAddSave = true;
  }

  update(passedOrders: any){
    this.apiOrders.updateOrders(passedOrders).subscribe(res => {
      this.toggleSuccess.emit(true);
      this.toggleError.emit(false);
      this.cellEditStopped = true;
      this.isSaveAttempted = false;
      this.valueChangedIds= [];
      this.ordersToSave[this.ordersToSave.length -1].newlyAdded = false;
      setTimeout(()=>{
        this.toggleSuccess.emit(false);
      }, 3000);
      this.toggleAddSave = false;
    }, err => {
      // Issues in Backend.
      this.toggleError.emit(true);
      this.cantSaveError.emit("Can't Save the Orders. "+err);
    }); 
  }

  // Update the row node to Backend.
  updateOrders(){
    this.isSaveAttempted = true;
    this.getAllRecords();
    if(this.validationPass){
      this.update(this.ordersToSave);
    }else {
      this.agGrid.api.redrawRows(); // If Validation fails.
    }
  }

  // Insert the entry with empty values.
  onCancel(){
    this.getAllRecords();
    var lastId = this.ordersToSave.length - 1;
    var rowNode = this.agGrid.api.getDisplayedRowAtIndex(lastId)!;
    this.agGrid.api.applyTransaction({remove: [rowNode.data]});
    this.deleted = true;
    this.toggleAddSave = this.cellValueChanged == false ? false : true;
  }

  // Delete entries.
  onDeleteRow(){
    Swal.fire({
      title: 'Are you sure to delete ?',
      text: 'This process is irreversible.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
    }).then((result) => {
      if (result.value) {
        var selectedOrders = this.agGrid.api.getSelectedRows ();
        selectedOrders.forEach(element => {
          // Don't send delete requests to backend if entry is freshly created.
          if(element.newlyAdded){
            this.agGrid.api.applyTransaction({remove: selectedOrders});
            this.deleted = true;
            this.toggleAddSave = this.cellValueChanged == false ? false : true;
            this.getAllRecords();
          }else{
            // Send delete request to Backend.
            this.apiOrders.deleteOrder(element.id).subscribe(res => {
              this.deleted = true;
              this.agGrid.api.applyTransaction({remove: selectedOrders});
              this.getAllRecords();
            }, err => {
              this.deleted = false;
              this.toggleError.emit(true);
              this.cantSaveError.emit("Can't delete the Order. "+err);
              //Orders not deleted in Server.
            });
          }
          const index = this.valueChangedIds.indexOf(element.id);
          if (index > -1) { // only splice array when item is found
            this.valueChangedIds.splice(index, 1); // 2nd parameter means remove one item only
          }
          if(this.valueChangedIds.length == 0){
            this.toggleAddSave = false;
          }
        });
      }
    });
  }
}
